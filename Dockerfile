FROM golang:1.18-alpine as buildbase

RUN apk add git build-base

WORKDIR /go/src/gitlab.com/transactions
COPY vendor .
COPY . .

RUN GOOS=linux go build  -o /usr/local/bin/transactions /go/src/gitlab.com/transactions


FROM alpine:3.9

COPY --from=buildbase /usr/local/bin/transactions /usr/local/bin/transactions
RUN apk add --no-cache ca-certificates

ENTRYPOINT ["transactions"]
