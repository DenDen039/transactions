package main

import (
	"os"

	"gitlab.com/transactions/internal/cli"
)

func main() {
	if !cli.Run(os.Args) {
		os.Exit(1)
	}
}
