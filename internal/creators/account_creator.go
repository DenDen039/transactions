package creators

import (
	"context"
	"fmt"

	"gitlab.com/distributed_lab/logan/v3/errors"
	"gitlab.com/tokend/go/xdrbuild"
	horizon "gitlab.com/tokend/horizon-connector"
	"gitlab.com/tokend/keypair"
)

type AccountCreator struct {
	Tx      *xdrbuild.Transaction
	Horizon *horizon.Connector
}
type AcountData struct {
	RoleID  uint64
	Signers []xdrbuild.SignerData
}

func NewAccountCreator(tx *xdrbuild.Transaction, connector *horizon.Connector) AccountCreator {
	return AccountCreator{
		Tx:      tx,
		Horizon: connector,
	}
}

func (creator AccountCreator) CreateAccount(account_id string, signers []xdrbuild.SignerData) error {
	tx := creator.Tx

	tx, _, err := creator.account(tx, account_id, signers)
	if err != nil {
		return errors.Wrap(err, "failed to craft account operation")
	}

	envelope, err := tx.Marshal()
	if err != nil {
		return errors.Wrap(err, "failed to build tx envelope")
	}

	result := creator.Horizon.Submitter().Submit(context.Background(), envelope)
	if result.Err != nil {

		fmt.Print(string(result.RawResponse))
		panic(result.Err)
		//return
	}

	return nil

}
func (creator AccountCreator) account(tx *xdrbuild.Transaction, account_id string, signers []xdrbuild.SignerData) (*xdrbuild.Transaction, uint64, error) {

	MasterAdressID := "GBA4EX43M25UPV4WIE6RRMQOFTWXZZRIPFAI5VPY6Z2ZVVXVWZ6NEOOB"
	tx = tx.Op(&xdrbuild.CreateAccount{
		Destination: account_id,
		RoleID:      1,
		Signers:     signers,
		Referrer:    &MasterAdressID,
	}).Sign(keypair.MustParseSeed("SAMJKTZVW5UOHCDK5INYJNORF2HRKYI72M5XSZCBYAHQHR34FFR4Z6G4"))
	return tx, 1, nil
}
