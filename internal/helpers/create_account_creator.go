package helpers

import (
	"fmt"
	"net/url"

	"gitlab.com/tokend/go/xdrbuild"
	horizon "gitlab.com/tokend/horizon-connector"
	"gitlab.com/tokend/keypair"
	"gitlab.com/transactions/internal/creators"
	"gitlab.com/transactions/internal/requests"
)

func NewAccountCreator(req requests.AccountCreateRequestInterface) creators.AccountCreator {
	builder := xdrbuild.NewBuilder(req.GetPassPhrase(), int64(req.GetExpirationPeriod()))
	tx := builder.Transaction(req.GetMasterAdressID())
	dev_editon_url, err := url.Parse("http://localhost:8000/_/api/")
	if err != nil {
		fmt.Println("Error")
	}
	return creators.NewAccountCreator(tx, horizon.NewConnector(dev_editon_url).WithSigner(keypair.MustParseSeed("SAMJKTZVW5UOHCDK5INYJNORF2HRKYI72M5XSZCBYAHQHR34FFR4Z6G4")))
}
