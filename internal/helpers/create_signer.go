package helpers

import (
	"encoding/json"

	"gitlab.com/tokend/go/xdrbuild"
)

type AccountSigner struct {
	SignerID string
	RoleID   uint64
	Weight   uint32
	Identity uint32
}
type Details map[string]interface{}

func (d Details) MarshalJSON() ([]byte, error) {
	return json.Marshal(map[string]interface{}(d))
}

func NewSigner(accountSigner AccountSigner) xdrbuild.SignerData {
	return xdrbuild.SignerData{
		PublicKey: accountSigner.SignerID,
		RoleID:    accountSigner.RoleID,
		Weight:    accountSigner.Weight,
		Identity:  accountSigner.Identity,
		Details:   Details{},
	}
}

func NewSignersList(signers ...AccountSigner) []xdrbuild.SignerData {
	var signers_list []xdrbuild.SignerData
	for _, signer := range signers {
		signers_list = append(signers_list, NewSigner(signer))
	}
	return signers_list
}
