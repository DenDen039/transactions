package requests

import (
	"gitlab.com/tokend/go/xdrbuild"
	"gitlab.com/tokend/keypair"
)

type AccountCreateRequestInterface interface {
	GetPassPhrase() string
	GetMasterAdressID() keypair.Address
	GetSigners() []xdrbuild.SignerData
	GetInfo() string
	GetExpirationPeriod() uint64
}

type AccountCreateRequest struct {
	MasterAdressID   string
	PassPhrase       string
	Signers          []xdrbuild.SignerData
	ExpirationPeriod uint64
	Info             string
}

func (c AccountCreateRequest) GetInfo() string {
	return c.Info
}
func (c AccountCreateRequest) GetExpirationPeriod() uint64 {
	return c.ExpirationPeriod
}
func (c AccountCreateRequest) GetPassPhrase() string {
	return c.PassPhrase
}

func (c AccountCreateRequest) GetMasterAdressID() keypair.Address {
	return keypair.MustParseAddress(c.MasterAdressID)
}

func (c AccountCreateRequest) GetSigners() []xdrbuild.SignerData {
	return c.Signers
}
