package service

import (
	"gitlab.com/distributed_lab/logan/v3"
	"gitlab.com/tokend/keypair"
	"gitlab.com/transactions/internal/config"
	"gitlab.com/transactions/internal/helpers"
	"gitlab.com/transactions/internal/requests"
)

const (
	MasterAdressID   string = "GBA4EX43M25UPV4WIE6RRMQOFTWXZZRIPFAI5VPY6Z2ZVVXVWZ6NEOOB"
	ExpirationPeriod uint64 = 601200
	PassPhrase       string = "TokenD Developer Network"
	DefaultRoleId    uint64 = 1
)

type service struct {
	log *logan.Entry
}

func (s *service) run() error {
	s.log.Info("Service started")
	account_id, _ := keypair.Random()
	signers := helpers.NewSignersList(
		helpers.AccountSigner{
			SignerID: account_id.Address(),
			RoleID:   DefaultRoleId,
			Weight:   1000,
			Identity: 1,
		})
	req := requests.AccountCreateRequest{
		MasterAdressID:   MasterAdressID,
		PassPhrase:       PassPhrase,
		Signers:          signers,
		ExpirationPeriod: ExpirationPeriod,
	}
	err := helpers.NewAccountCreator(req).CreateAccount(account_id.Address(), signers)
	if err != nil {
		panic(err)
	}
	return nil
}

func newService(cfg config.Config) *service {
	return &service{
		log: cfg.Log(),
	}
}

func Run(cfg config.Config) {
	if err := newService(cfg).run(); err != nil {
		panic(err)
	}
}
